package ru.t1.sukhorukova.tm.component;

import ru.t1.sukhorukova.tm.api.controller.ICommandController;
import ru.t1.sukhorukova.tm.api.controller.IProjectController;
import ru.t1.sukhorukova.tm.api.controller.IProjectTaskController;
import ru.t1.sukhorukova.tm.api.controller.ITaskController;
import ru.t1.sukhorukova.tm.api.repository.ICommandRepository;
import ru.t1.sukhorukova.tm.api.repository.IProjectRepository;
import ru.t1.sukhorukova.tm.api.repository.ITaskRepository;
import ru.t1.sukhorukova.tm.api.service.*;
import ru.t1.sukhorukova.tm.constant.ArgumentConst;
import ru.t1.sukhorukova.tm.constant.CommandConst;
import ru.t1.sukhorukova.tm.controller.CommandController;
import ru.t1.sukhorukova.tm.controller.ProjectController;
import ru.t1.sukhorukova.tm.controller.ProjectTaskController;
import ru.t1.sukhorukova.tm.controller.TaskController;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.sukhorukova.tm.exception.system.CommandNotSupportedException;
import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.repository.CommandRepository;
import ru.t1.sukhorukova.tm.repository.ProjectRepository;
import ru.t1.sukhorukova.tm.repository.TaskRepository;
import ru.t1.sukhorukova.tm.service.*;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ICommandService commandService = new CommandService(commandRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final ILoggerService loggerService = new LoggerService();

    private final ICommandController commandController = new CommandController(commandService);
    private final ITaskController taskController = new TaskController(taskService);
    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);
    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    public void start(final String[] args) {
        initLogger();
        processArguments(args);
        initDemoData();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final RuntimeException e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void processArguments(final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            processArgument(args[0]);
            System.out.println("[OK]");
            System.exit(0);
        } catch (final RuntimeException e) {
            System.out.println(e.getMessage());
            System.out.println("[FAIL]");
            System.exit(1);
        }
    }

    private void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) throw new ArgumentNotSupportedException(arg);
        switch (arg) {
            case ArgumentConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.CMD_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.CMD_INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.CMD_ARGUMENT:
                commandController.showArguments();
                break;
            case ArgumentConst.CMD_COMMAND:
                commandController.showCommands();
                break;
            default:
                throw new ArgumentNotSupportedException(arg);
        }
    }

    private void processCommand(final String command) {
        if (command == null || command.isEmpty()) throw new CommandNotSupportedException();
        switch (command) {
            case CommandConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case CommandConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.CMD_HELP:
                commandController.showHelp();
                break;
            case CommandConst.CMD_INFO:
                commandController.showInfo();
                break;
            case CommandConst.CMD_EXIT:
                exit();
                break;
            case CommandConst.CMD_ARGUMENT:
                commandController.showArguments();
                break;
            case CommandConst.CMD_COMMAND:
                commandController.showCommands();
                break;
            case CommandConst.CMD_PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.CMD_PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.CMD_PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConst.CMD_PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConst.CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConst.CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConst.CMD_PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConst.CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConst.CMD_PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.CMD_PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CommandConst.CMD_PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CommandConst.CMD_PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case CommandConst.CMD_PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case CommandConst.CMD_PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case CommandConst.CMD_PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case CommandConst.CMD_TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.CMD_TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.CMD_TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case CommandConst.CMD_TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CommandConst.CMD_TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConst.CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConst.CMD_TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CommandConst.CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CommandConst.CMD_TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConst.CMD_TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CommandConst.CMD_TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case CommandConst.CMD_TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case CommandConst.CMD_TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case CommandConst.CMD_TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case CommandConst.CMD_TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case CommandConst.CMD_TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case CommandConst.CMD_TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case CommandConst.CMD_TASK_SHOW_BY_PROJECT_ID:
                taskController.showTasksByProjectId();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

     private void exit() {
        System.exit(0);
    }

    private void initDemoData() {
        projectService.add(new Project("PROJECT_01", "Test project 1", Status.COMPLETED));
        projectService.add(new Project("PROJECT_18", "Test project 18", Status.IN_PROGRESS));
        projectService.add(new Project("PROJECT_02", "Test project 2", Status.NOT_STARTED));
        projectService.add(new Project("PROJECT_26", "Test project 26", Status.IN_PROGRESS));

        taskService.add(new Task("TASK_01", "Test task 1", Status.COMPLETED));
        taskService.add(new Task("TASK_18", "Test task 18", Status.IN_PROGRESS));
        taskService.add(new Task("TASK_02", "Test task 2", Status.NOT_STARTED));
        taskService.add(new Task("TASK_26", "Test task 26", Status.IN_PROGRESS));
    }

}
