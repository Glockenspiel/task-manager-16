package ru.t1.sukhorukova.tm.constant;

public final class ArgumentConst {

    public static final String CMD_HELP = "-h";
    public static final String CMD_VERSION = "-v";
    public static final String CMD_ABOUT = "-a";
    public static final String CMD_INFO = "-i";
    public static final String CMD_ARGUMENT = "-arg";
    public static final String CMD_COMMAND = "-cmd";

}
